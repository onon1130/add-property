<?php include 'DB_connect.php'; ?>
<?php
extract($_GET);

if (!$subDistrictID) {
  $sql = "SELECT * FROM Estate WHERE sub_district_ID IN (SELECT sub_district_ID FROM SubDistrict WHERE district_ID='$districtID') ";
} else {
  $sql = "SELECT * FROM Estate WHERE sub_district_ID='$subDistrictID' ";
}

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  ?>
  <select class="form-control" id="estate" name="estate_id">
    <option value="">All Estate</option>
    <?php
    // output data of each row
    while ($row = $result->fetch_assoc()) {
      echo "<option value='" . $row["estate_ID"] . "'>" . $row["estate_name"] . "</option>";
    }
    ?></select>

  <?php
} else {
  echo "- No estate found -";
}
$conn->close();
?>