<?php include 'DB_connect.php'; ?>
<?php
$branch_id = $_REQUEST["branch_id"];
$branch_name = $_REQUEST["branch_name"];
$reportPeriod = $_REQUEST["reportPeriod"];
echo "<!-- branch_id :" . $branch_id . "<br />-->";
echo "<!-- branch_name :" . $branch_name . "<br />-->";
echo "<!-- reportPeriod :" . $reportPeriod . "<br />-->";
$sql = "SELECT"
        . " BH.branch_ID, BH.branch_name,"
        . " SM.staff_name as branch_manager,"
        . " SUM(TX.transaction_price) AS total_tx_price,"
        . " SUM(TX.commission) AS total_comm,"
        . " COUNT(TX.transaction_type) AS total_count,"
        . " TX.transaction_type"
        . " FROM Branch as BH"
        . " INNER JOIN Staff as SF on SF.branch_ID = BH.branch_ID AND SF.staff_type ='agent'"
        . " INNER JOIN Staff as SM on SM.branch_ID = BH.branch_ID AND SM.staff_type ='manager'"
        . " INNER JOIN Transaction as TX on TX.agent_ID = SF.staff_ID";
if (!empty($branch_id)) {
  $sql = $sql . " WHERE SF.branch_id = '$branch_id'";
}
if (!empty($reportPeriod)) {
  $sql = $sql . " AND MONTH(TX.transaction_date) = MONTH(CURRENT_DATE())"
          . " AND YEAR(TX.transaction_date) = YEAR(CURRENT_DATE())";
}


$sql = $sql . " GROUP BY TX.transaction_type, SF.branch_ID";
$sql = $sql . " ORDER BY  SF.branch_ID, TX.transaction_type";
echo "<!-- SQL :" . $sql . "-->";
$result = $conn->query($sql);
$resultCount = $result->num_rows;
?>
<!DOCTYPE html>
<html lang="en">
  <?php include 'head.php'; ?>
  <body>
    <!-- Navigation -->
    <?php include 'nav.php'; ?>
    <!-- Header - set the background image for the header in the line below -->
    <header class="bg-image-full header-tab header-tab-page">
      <div class="container">
        <div class="header-wrapper">
          <nav aria-label="breadcrumb" role="navigation" class="page-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/ADD-property/">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">
                Report of Transaction for 
                <?php if ($branch_id) {
                  echo '<strong>' . $branch_name . '</strong>';
                } else {
                  echo '<strong> All Branches</strong>';
                } ?>   
                on <?php if (!empty($reportPeriod)) {
                  echo '<strong> Current Month</strong>';
                } else {
                  echo '<strong> All Transactions</strong>';
                } ?>
            </ol>
          </nav>
        </div>
      </div>
    </header>
    <div class="container">
      <?php
      if ($resultCount > 0) {
        $rowNum = 0;
        $preBranchID = "default";
        ?>

  <?php
  while ($row = $result->fetch_assoc()) {
    $rowNum += 1;
    if ($preBranchID <> $row["branch_ID"]) {
      if ($preBranchID <> "default" ) {
      ?>
    </tbody>
    </table>
</div>
      <?php }
              $preBranchID = $row["branch_ID"];
      ?>
    <div class="report-wrapper">
      <div class="customerInfo">
        <h6> Branch</h6>
          <div class="row">
            <div class="col-sm-12 col-md-4">
            <div class="col-sm-3"><span>ID: </span></div>
            <div class="col-sm-9"><?php echo $row["branch_ID"]?></div>
            <div class="col-sm-3"><span> Name:</span></div>
            <div class="col-sm-9"><?php echo $row["branch_name"] ?></div>
            <div class="col-sm-3"><span>Manager:</span></div>
            <div class="col-sm-9"> <?php echo $row["branch_manager"] ?></div>
          </div>
        </div>
      </div>
      <table class="table table-hover table-property">
        <thead>
          <tr>
            <th scope="col">Transaction Type</th>
            <th scope="col">Transaction Price</th>
            <th scope="col">Commission</th>
            <th scope="col">No. of transaction</th>
          </tr>
        </thead>
        <tbody>
      <?php } ?>
          <tr>
            <td><?php echo $row["transaction_type"] ?></td>
            <td>$<?php echo number_format($row["total_tx_price"]) ?></td>
            <td>$<?php echo number_format($row["total_comm"]) ?></td>
            <td><?php echo $row["total_count"] ?></td>
          </tr>
      <?php 
     }?>
        </tbody>
      </table>
      </div>
          <?php
} else {
  echo "<div class='noRecord-box'>Sorry, No records found.</div>";
}
$conn->close();
?>
          </div>
          <!-- Footer -->
<?php include 'footer.php'; ?>
          </body>
        <script>
          $(document.body).on('click', '.viewProperty', function (e) {
            e.preventDefault();
            var cust_id = $(this).data('cust-id');
            var cust_name = $('#' + cust_id).data('cust-name');
            var cust_contact_num = $('#' + cust_id).data('cust-contact-num');
            var district_id = $('#' + cust_id).data('district-id');
            var sub_district_id = $('#' + cust_id).data('sub-district-id');
            var estate_id = $('#' + cust_id).data('estate-id');
            var selling_price = $('#' + cust_id).data('budget-buy');
            var rental_price = $('#' + cust_id).data('budget-rent');
            var propertyType = $(this).data('property-type');
            redirectURL = 'property_list.php?'
                    + 'cust_id=' + cust_id
                    + '&cust_name=' + cust_name
                    + '&cust_contact_num=' + cust_contact_num
                    + '&district_id=' + district_id
                    + '&sub_district_id=' + sub_district_id
                    + '&estate_id=' + estate_id
                    + '&selling_price=' + selling_price
                    + '&rental_price=' + rental_price
                    + '&propertyType=' + propertyType;
            window.location = redirectURL;
          });
        </script>
        </html>
