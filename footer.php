<footer class="bg-dark-trans">
  <div class="container">
    <div class="footer_content">
      <p class="m-0 text-center text-white">Copyright &copy; Oscar On 2017</p>
    </div>
  </div>
  <!-- /.container -->
</footer>
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
  $(document).ready(function () {
    $('#btn-login').click(function () {
      var username = $('#username').val();
      var password = $('#password').val();
      if (username != '' && password != '') {
        $.ajax({
          url: 'AjaxLoginAction.php',
          method: "POST",
          data: ({username: username, password: password}),
          success: function (data) {
            if (data == 'Yes') {
              $('#login-error span').html('').hide()
              $("#loginModal").modal('hide');
              location.reload();
            } else {
              //alert("Wrong data");
              $('#login-error span').html('Incorrect username or password!').fadeIn();
            }
          }
        });
      } else {
        $('#login-error span').html('Please enter both username and password!').fadeIn();
        //alert("Both Field are required");
      }
    });

    $('#btn-logout').click(function () {
      var action = "logout";
      $.ajax({
        url: 'AjaxLoginAction.php',
        method: "POST",
        data: ({action: action}),
        success: function (data) {
          location.reload();
        }
      });
    });
    
    $(document.body).on('click', '.viewProperty', function (e) {
    e.preventDefault();
    var cust_id = $(this).data('cust-id');
    var propertyType = $(this).data('property-type'); 
    redirectURL= 'property_list.php?' 
            + 'cust_id=' +cust_id
            + '&propertyType=' +propertyType;
    //console.log(redirectURL);
    window.location = redirectURL;
  });
  });
  function checkLogin(username, password) {
    $.ajax({
      url: 'AjaxLoginAction.php',
      method: "POST",
      data: ({username: username, password: password}),
      success: function (data) {
        return data;

      }
    });
  }
</script>