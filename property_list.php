<?php include 'DB_connect.php'; ?>
<?php

$propertyType = $_REQUEST["propertyType"];
$district_id = $_REQUEST["district_id"];
$sub_district_id = $_REQUEST["sub_district_id"];
$estate_id = $_REQUEST["estate_id"];
$estate_name = $_REQUEST["estate_name"];
echo "<!-- propertyType :" . $propertyType . "<br />-->";
echo "<!-- district_id :" . $district_id . "<br />-->";
echo "<!-- sub_district_id :" . $sub_district_id . "<br />-->";
echo "<!-- estate_id :" . $estate_id . "<br />-->";
echo "<!-- estate_name :" . $estate_name . "<br />-->";
// from customer page
$cust_id = $_REQUEST["cust_id"];
echo "<!-- cust_id :" . $cust_id . "<br />-->";

$sql = "SELECT"
        . " PT.*,"
        . " OW.owner_name,"
        . " OW.owner_contact_num,"
        . " ES.estate_name,"
        . " SD.sub_district_name,"
        . " DT.district_name"
        . " FROM District as DT"
        . " INNER JOIN SubDistrict as SD on SD.district_ID = DT.district_ID"
        . " INNER JOIN Estate as ES on ES.sub_district_ID = SD.sub_district_ID"
        . " INNER JOIN Property as PT on PT.estate_ID = ES.estate_ID"
        . " INNER JOIN Owner as OW on OW.owner_ID = PT.owner_ID"
        . " WHERE PT.status='available'";
if (!empty($sub_district_id)) {
  if (empty($estate_id)) {
      $sql = $sql . " AND SD.sub_district_ID='$sub_district_id')";
      
  } else {
    $sql = $sql . " AND PT.estate_ID='$estate_id'";
  }
} else {
  if (!empty($district_id)) {
      $sql = $sql . " AND DT.district_ID = '$district_id'";
  }
}
if (!empty($district_id)) {
}
if (empty($estate_id) && !empty($estate_name)) {
        $sql = $sql . " AND ES.estate_name like '%$estate_name%'";
}

if ($propertyType == 'rent') {
  $sql = $sql . " AND PT.rental_price IS NOT NULL";
} else if ($propertyType == 'sell') {
  $sql = $sql . " AND PT.selling_price IS NOT NULL";
}

// for customer preference search
if (!empty($cust_id)){
    $sqlCustomer = "SELECT"
        . " CS.*,"
        . " ES.estate_name AS preferred_estate_name,"
        . " SD.sub_district_name AS preferred_sub_district_name,"
        . " DT.district_name AS preferred_district_name "
        . " FROM Customer as CS"
        . " LEFT JOIN District as DT on DT.district_ID = CS.pref_district_ID"
        . " LEFT JOIN SubDistrict as SD on SD.sub_district_ID = CS.pref_sub_district_ID"
        . " LEFT JOIN Estate as ES on ES.estate_ID = CS.pref_estate_ID"
        . " WHERE CS.cust_ID='$cust_id'";
    $resultCust = $conn->query($sqlCustomer);
    $rowCust = $resultCust->fetch_assoc();
    $cust_name = $rowCust['cust_name'];
    $cust_contact_num = $rowCust['cust_contact_num'];
    $selling_price = $rowCust['budget_buy'];
    $rental_price = $rowCust['budget_rent'];
    $preferred_estate_name = $rowCust['preferred_estate_name'];
    $preferred_sub_district_name = $rowCust['preferred_sub_district_name'];
    $preferred_district_name = $rowCust['preferred_district_name'];
   
  $sql = $sql . " AND"
          . " (CASE WHEN (Select pref_estate_ID FROM Customer WHERE cust_ID = '$cust_id') IS NOT NULL" 
          . " THEN PT.estate_ID = (Select pref_estate_ID FROM Customer WHERE cust_ID = '$cust_id')"
          . " ELSE TRUE END)";
  $sql = $sql . " AND"
          . " (CASE WHEN (Select pref_sub_district_ID FROM Customer WHERE cust_ID = '$cust_id') IS NOT NULL" 
          . " THEN SD.sub_district_ID = (Select pref_sub_district_ID FROM Customer WHERE cust_ID = '$cust_id')"
          . " ELSE TRUE END)";
  $sql = $sql . " AND"
          . " (CASE WHEN (Select pref_district_ID FROM Customer WHERE cust_ID = '$cust_id') IS NOT NULL" 
          . " THEN DT.district_ID = (Select pref_district_ID FROM Customer WHERE cust_ID = '$cust_id')"
          . " ELSE TRUE END)";
  if ($propertyType == 'rent') {
    $sql = $sql . " AND PT.rental_price <= (SELECT budget_rent FROM Customer WHERE cust_ID = '$cust_id')";
  } else if ($propertyType == 'sell') {
    $sql = $sql . " AND PT.selling_price <= (SELECT budget_buy FROM Customer WHERE cust_ID = '$cust_id')";
  }
          
}
echo "<!-- SQL :" . $sql . "-->";
$result = $conn->query($sql);
$resultCount = $result->num_rows;
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'; ?>
  <body>
    <!-- Navigation -->
    <?php include 'nav.php'; ?>
    <!-- Header - set the background image for the header in the line below -->
    <header class="bg-image-full header-tab header-tab-page">
      <div class="container">
        <div class="header-wrapper">
          <nav aria-label="breadcrumb" role="navigation" class="page-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/ADD-property/">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">
                Property for 
                <?php if (!empty($cust_id)) { ?>
                <!-- title for customer preference search start-->
                Recommended property for Customer
                <!-- title for customer preference search end-->
                <?php } else { ?>
                <!-- title for property  search start-->
                <strong><?php echo $propertyType; ?></strong>
                <?php if ($estate_name) { echo ' of  <strong>'. $estate_name .'</strong>';}?> 
              <?php if ($sub_district_name) { echo ' within <strong>'. $sub_district_name .'</strong>, ';} else { if ($district_name) {echo " within";}; }?>
              <?php if ($district_name) { echo  ' <strong>'. $district_name .'</strong>';}?>
              <!-- title for property  search end-->
              </li>
            <?php } ?>
            </ol>
          </nav>
        </div>
      </div>
    </header>
    <div class="container">
      <?php if (!empty($cust_id)) { ?>
      <!-- for customer preference search start-->
      <div class="customerInfo">
      <h6> Customer</h6>
      <div class="row">
        <div class="col-sm-12 col-md-4">
            <div class="col-sm-3"><span>ID: </span></div>
            <div class="col-sm-9"><?php echo $cust_id?></div>
            <div class="col-sm-3"><span> Name:</span></div>
            <div class="col-sm-9"><?php echo $cust_name ?></div>
            <div class="col-sm-3"><span>Contact:</span></div>
            <div class="col-sm-9"> <?php echo $cust_contact_num ?></div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="col-sm-3"><span>Estate/ district:</span></div>
            <div class="col-sm-9"><?php if ($preferred_estate_name && $preferred_estate_name <> NULL) { echo $preferred_estate_name .'<br />';}?> 
              <?php if ($preferred_district_name && $preferred_district_name <> NULL) { echo $preferred_district_name ;
              if ($preferred_sub_district_name) {
              echo ", "; } }?>
              <?php if ($preferred_sub_district_name && $preferred_sub_district_name <> NULL) { echo $preferred_sub_district_name ;}?>
             <?php if (!($preferred_estate_name) && !($preferred_sub_district_name) &&!($preferred_district_name) ) echo "--"?>
            </div>
        </div>
       <div class="col-sm-12 col-md-4">
        <span>Budget for</span>Buy : 
        <?php if (!isset($selling_price) || trim($selling_price) === '' || $selling_price === 0) {
              echo "--";
            } else { ?>
              <a href="#" class="viewProperty" data-cust-id="<?php echo $cust_id ?>" data-property-type="sell">
                  <?php echo  number_format($selling_price)?>
              </a>
       <?php } ?>
       <br /><span>Budget for</span> Rent :
       <?php if (!isset($rental_price) || trim($rental_price) === '' || $rental_price === 0) {
              echo "--";
            } else { ?>
              <a href="#" class="viewProperty" data-cust-id="<?php echo $cust_id ?>" data-property-type="rent">
                  <?php echo number_format($rental_price)?>
              </a>
                 
       <?php } ?> 
       
        </div>
        </div>
      </div>
      <br /><h8> Property(s) for
        <strong><?php if ($propertyType == 'rent') {
          echo "Rental";
        }
          else if ($propertyType == 'sell') {
            echo "Sale";
        }
        ?>
        </strong>
        match for this customer :</h8>
       <!-- for customer preference search end-->
      <?php }
      if ($resultCount > 0) {
        $rowNum = 0;
      ?>
        <table class="table table-hover table-property">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Property</th>
              <th scope="col">District</th>
              <th scope="col">Estate</th>
              <th scope="col"></th>
              <th scope="col">Floor Area</th>
              <th scope="col">Rooms & others</th>
              <th scope="col">Selling $</th>
              <th scope="col">Rental $</th>
              <th scope="col">Remark</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while ($row = $result->fetch_assoc()) {
              $rowNum += 1;
              ?>
              <tr>
                <th scope="row"><?php echo $rowNum ?></th>
                 <td nowrap>
               <?php if (isset($_SESSION["username"])) { ?>
                  <!-- access for agents, managers or admin start  -->
                  <span>ID: </span><?php echo $row["property_ID"] ?><br />
                  <span>Owner: </span> <?php echo $row["owner_name"] ?><br />
                  <span>Contact: </span> <?php echo $row["owner_contact_num"] ?>
                  <!-- access for agents, managers or admin end  -->
                <?php } else {?>
                  <?php echo $row["property_ID"] ?>
                <?php } ?>
                </td>
                <td><?php echo $row["district_name"] ?>,  <?php echo $row["sub_district_name"] ?></td>
                <td><?php echo $row["estate_name"] ?></td>
                <td nowrap>
                  <span>Block </span><?php if (!isset($row["block_num"]) || trim($row["block_num"]) === '') {
                  echo "--";
                  } else {
                    echo $row["block_num"];
                  } ?><br />
                          <span> Floor </span><?php if (!isset($row["floor_num"]) || trim($row["floor_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["floor_num"];
                  } ?><br />
                          <span>Flat </span><?php if (!isset($row["flat_num"]) || trim($row["flat_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["flat_num"];
                  } ?>
                </td>
                <td nowrap>
                          <span>Gross:</span> <?php if (!isset($row["gross_area"]) || trim($row["gross_area"]) === '' || $row["gross_floor_area"] === 0) {
                    echo "--";
                  } else {
                    echo $row["gross_area"] . "<span> sqft</span>";
                  } ?><br/>
                          <span>Net: </span><?php if (!isset($row["net_area"]) || trim($row["net_area"]) === '' || $row["net_floor_area"] === 0) {
                    echo "--";
                  } else {
                    echo $row["net_floor_area"] . "<span> sqft</span>";
                  } ?>
                </td>
                <td nowrap>
                  <span>No. of living rooms: </span><?php if (!isset($row["livingroom"]) || trim($row["num_of_livingroom"]) === '') {
                    echo "--";
                  } else {
                    echo $row["livingroom_num"];
                  } ?><br/>
                  <span>No. of bedrooms: </span><?php if (!isset($row["bedroom_num"]) || trim($row["bedroom_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["bedroom_num"];
                  } ?><br/>
                              <span>No. of bathrooms: </span><?php if (!isset($row["bathroom_num"]) || trim($row["bathroom_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["bathroom_num"];
                  } ?><br/>
                              <span>Kitchen style: </span><?php if (!isset($row["kitchen"]) || trim($row["kitchen"]) === '') {
                    echo "--";
                  } else {
                    echo $row["kitchen"];
                  } ?><br/>
                              <span>Car Park provided: </span><?php if (!isset($row["carpark"]) || trim($row["carpark"]) === '') {
                    echo "No";
                  } else {
                    echo "Yes";
                  } ?>
                </td>
                <td class="td-price">
                <?php if (!isset($row["selling_price"]) || trim($row["selling_price"]) === '' || $row["selling_price"] === 0) {
                  echo "--";
                } else {
                  echo "<span> $</span>" . number_format($row["selling_price"]);
                } ?>
                <?php if (trim($row["status"]) === 'sold') {
                  echo "<br /><span class='statusAlert'> Sold</span>";
                } ?>
               </td>
               <td class="td-price">
                <?php if (!isset($row["rental_price"]) || trim($row["rental_price"]) === '' || $row["rental_price"] === 0) {
                  echo "--";
                } else {
                  echo "<span> $</span>" . number_format($row["rental_price"]);
                } ?>
                <?php if (trim($row["status"]) === 'rentOut') {
                  echo "<br /><span class='statusAlert'> Rent out</span>";
                } ?>
                </td>
                <td><span><?php if (!isset($row["remarks"]) || trim($row["remarks"]) === '' || $row["remarks"] === 0) {
                    echo "--";
                  } else {
                    echo $row["remarks"];
                  } ?></span>
                </td>
              </tr>

          <?php } ?>
          </tbody>
        </table>
          <?php
        } else {
          echo "<div class='noRecord-box'>Sorry, No records found.</div>";
        }
        $conn->close();
        ?>
    </div>
    <!-- Footer -->
    <?php include 'footer.php'; ?>
  </body>
</html>
