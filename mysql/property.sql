-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2017 at 01:20 AM
-- Server version: 5.6.36
-- PHP Version: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `property`
--

-- --------------------------------------------------------

--
-- Table structure for table `Branch`
--

CREATE TABLE `Branch` (
  `branch_ID` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `branch_name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Branch`
--

INSERT INTO `Branch` (`branch_ID`, `branch_name`, `branch_address`) VALUES
('B0001', 'Kowloon Bay Branch', 'Shop P999, Telford Gardens, Kowloon Bay, Kowloon '),
('B0002', 'Mong Kok Branch', '999A Sai Yeung Choi St, Mong Kok, Kowloon '),
('B0003', 'Kowloon Station Branch', 'Shop A10,Kowloon MTR Station, Kowloon');

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `cust_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cust_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_contact_num` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pref_district_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pref_sub_district_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pref_estate_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `budget_buy` int(10) DEFAULT NULL,
  `budget_rent` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`cust_ID`, `cust_name`, `cust_contact_num`, `pref_district_ID`, `pref_sub_district_ID`, `pref_estate_ID`, `budget_buy`, `budget_rent`) VALUES
('C000000001', 'Oscar On', '98888888', NULL, NULL, NULL, 60000000, 120000),
('C000000002', 'Ms Lee', '83939345', 'D0010', NULL, NULL, NULL, 16000),
('C000000003', 'Ms Cheung', '67383838', 'D0009', 'SD065', 'E000000002', 5000000, 14000),
('C000000004', 'Johnson Yip', '59393839', NULL, NULL, NULL, NULL, 12000),
('C000000005', 'Diana Tam', '93039393', 'D0009', 'SD065', 'E000000001', 4000000, NULL),
('C000000006', 'Mr Chan', '83939393', 'D0009', 'SD065', 'E000000003', NULL, NULL),
('C000000007', 'Mr Lee', '63832939', 'D0009', 'SD065', 'E000000002', 3500000, NULL),
('C000000008', 'Wong Siu Yu', '89330202', NULL, NULL, NULL, 3000000, NULL),
('C000000009', 'Elaine Lam', '39384949', 'D0009', NULL, NULL, NULL, 17000),
('C000000010', 'Fanny Ng', '94958594', 'D0009', NULL, NULL, 6500000, NULL),
('C000000011', 'Jenny Lui', '63039505', 'D0009', 'SD065', 'E000000002', NULL, NULL),
('C000000012', 'Karen Wong', '92029393', 'D0009', 'SD065', 'E000000003', NULL, NULL),
('C000000013', 'Louis Chan', '89202920', 'D0009', 'SD065', NULL, 4000000, NULL),
('C000000014', 'Michael Lee', '99338339', 'D0009', 'SD065', NULL, NULL, 13000),
('C000000015', 'Nicole Chan', '93839494', 'D0009', 'SD065', NULL, NULL, NULL),
('C000000016', 'Oscar Chan', '94848934', 'D0009', 'SD065', NULL, NULL, NULL),
('C000000017', 'Wong Siu Yu', '89202929', 'D0009', 'SD065', NULL, NULL, NULL),
('C000000018', 'Cheung Ka Ming', '97758758', 'D0009', 'SD065', NULL, NULL, NULL),
('C000000019', 'Chan Shui Ying', '69838380', 'D0009', 'SD065', NULL, NULL, NULL),
('C000000020', 'Ng Mei Lei', '60293933', 'D0009', 'SD065', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `District`
--

CREATE TABLE `District` (
  `district_ID` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `district_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `District`
--

INSERT INTO `District` (`district_ID`, `district_name`) VALUES
('D0001', 'Central and Western'),
('D0002', 'Wan Chai'),
('D0003', 'Eastern'),
('D0004', 'Southern'),
('D0005', 'Yau Tsim Mong'),
('D0006', 'Sham Shui Po'),
('D0007', 'Kowloon City'),
('D0008', 'Wong Tai Sin'),
('D0009', 'Kwun Tong'),
('D0010', 'Kwai Tsing'),
('D0011', 'Tsuen Wan'),
('D0012', 'Tuen Mun'),
('D0013', 'Yuen Long'),
('D0014', 'North'),
('D0015', 'Tai Po'),
('D0016', 'Sha Tin'),
('D0017', 'Sai Kung'),
('D0018', 'Islands');

-- --------------------------------------------------------

--
-- Table structure for table `Estate`
--

CREATE TABLE `Estate` (
  `estate_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estate_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_district_ID` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Estate`
--

INSERT INTO `Estate` (`estate_ID`, `estate_name`, `sub_district_ID`) VALUES
('E000000001', 'Tak Bo Garden', 'SD065'),
('E000000002', 'Amoy Gardens', 'SD065'),
('E000000003', 'Telford Garden', 'SD065'),
('E000000004', 'Jade Field Garden', 'SD065'),
('E000000005', 'Harbour Green', 'SD038'),
('E000000006', 'One SilverSea', 'SD038'),
('E000000007', 'Hermitage', 'SD038'),
('E000000008', 'Island Harbourview', 'SD038'),
('E000000009', 'Waterfront', 'SD033');

-- --------------------------------------------------------

--
-- Table structure for table `Owner`
--

CREATE TABLE `Owner` (
  `owner_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `owner_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_contact_num` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Owner`
--

INSERT INTO `Owner` (`owner_ID`, `owner_name`, `owner_contact_num`) VALUES
('W000000001', 'Cheung Siu Ming', '68860001'),
('W000000002', 'Chan Tai Man', '92332321'),
('W000000003', 'Lee Ka Keung', '68237823'),
('W000000004', 'Michael Tsang', '69283838'),
('W000000005', 'Chan Siu Mei', '92838383'),
('W000000006', 'Cheung Tak Ming', '61928282'),
('W000000007', 'ABC Trading Ltd.', '90233232'),
('W000000008', 'Oscar On', '98888888'),
('W000000009', 'Wong May', '69282280'),
('W000000010', 'Sammy Leung', '59238333'),
('W000000011', 'Polly Tang', '94049393'),
('W000000012', 'Queenie Li', '65958494'),
('W000000013', 'Ricky Cheung', '63920292'),
('W000000014', 'Sammi Tam', '95034939'),
('W000000015', 'Terry Yeung', '50293839'),
('W000000016', 'Ng Ka Wing', '89876898'),
('W000000017', 'Chan Tak Sum', '69320292'),
('W000000018', 'Chung Ka Yan', '96494849'),
('W000000019', 'Cheng Wai Ming', '98303930'),
('W000000020', 'Man Ching Kit', '29383030'),
('W000000021', 'Kung Ka Yan', '67390029'),
('W000000022', 'Wong Chio', '90003333'),
('W000000023', 'Tang Ka Yan', '80002029'),
('W000000024', 'Lam Chun Kit', '92020202'),
('W000000025', 'Wu Mei Yi', '93032802'),
('W000000026', 'Leung Man Wai', '69282011'),
('W000000027', 'Philip Lam', '50923211'),
('W000000028', 'Steven Tong', '30292022'),
('W000000029', 'Wong Siu Yu', '89202929'),
('W000000030', 'Chan Yat Man', '93033300'),
('W000000031', 'Chan Yee Man', '90393939'),
('W000000032', 'Ng Siu Wah', '92829383'),
('W000000033', 'Lee Man Kwong', '69393929'),
('W000000034', 'Yeung Siu Ming', '60929393'),
('W000000035', 'Mike Cheung', '64999393'),
('W000000036', 'Lisa Wong', '93039030'),
('W000000037', 'Michelle Lam', '92202000'),
('W000000038', 'William Chan', '89330202'),
('W000000039', 'John Lam', '50505050'),
('W000000040', 'Eric Siu', '33020202'),
('W000000041', 'Daniel Ng', '87484938');

-- --------------------------------------------------------

--
-- Table structure for table `Property`
--

CREATE TABLE `Property` (
  `property_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `owner_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estate_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `block_num` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `floor_num` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flat_num` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gross_area` int(5) DEFAULT NULL,
  `net_area` int(5) DEFAULT NULL,
  `bedroom_num` int(2) DEFAULT NULL,
  `livingroom_num` int(2) DEFAULT NULL,
  `bathroom_num` int(2) DEFAULT NULL,
  `kitchen` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carpark` tinyint(1) DEFAULT NULL,
  `selling_price` int(10) DEFAULT NULL,
  `rental_price` int(10) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Property`
--

INSERT INTO `Property` (`property_ID`, `owner_ID`, `estate_ID`, `block_num`, `floor_num`, `flat_num`, `gross_area`, `net_area`, `bedroom_num`, `livingroom_num`, `bathroom_num`, `kitchen`, `carpark`, `selling_price`, `rental_price`, `status`, `remarks`) VALUES
('P000000001', 'W000000008', 'E000000001', 'A', '15', 'B', 336, 265, 2, 1, 1, 'open', 1, 4200000, NULL, 'available', ''),
('P000000002', 'W000000001', 'E000000001', 'B', '9', 'E', 397, 282, 2, 1, 1, NULL, NULL, 4090000, NULL, 'sold', ''),
('P000000003', 'W000000005', 'E000000001', 'C', '10', 'F', 528, 382, 2, 2, 2, NULL, NULL, 5300000, NULL, 'sold', 'There are open, large single-sided vision'),
('P000000004', 'W000000003', 'E000000001', 'H', '30', 'D', 344, 265, 0, 1, 1, 'open', 1, 4250000, 12000, 'rentOut', ''),
('P000000005', 'W000000008', 'E000000002', 'A', '18', 'E', 484, 378, 2, 2, 1, NULL, NULL, 5500000, 14200, 'available', ''),
('P000000006', 'W000000004', 'E000000002', 'B', '18', 'E', 482, 375, 2, NULL, NULL, NULL, NULL, 5200000, NULL, 'sold', ''),
('P000000007', 'W000000007', 'E000000002', 'C', '25', 'B', 344, 265, 2, NULL, NULL, NULL, NULL, 5000000, NULL, 'sold', ''),
('P000000008', 'W000000008', 'E000000001', 'A', '30', 'E', 397, 282, 2, NULL, NULL, NULL, NULL, NULL, 12000, 'rentOut', ''),
('P000000009', 'W000000010', 'E000000002', 'G', '5', 'F', 482, 375, 2, NULL, NULL, NULL, NULL, 4900000, NULL, 'sold', ''),
('P000000010', 'W000000012', 'E000000004', '', '18', 'B', 478, 403, 2, NULL, NULL, NULL, NULL, NULL, 16000, 'rentOut', ''),
('P000000011', 'W000000008', 'E000000002', 'E', '5', 'F', 484, 378, 2, NULL, NULL, 'open', NULL, 7380000, 0, 'sold', ''),
('P000000012', 'W000000009', 'E000000001', 'C', '12', 'E', NULL, 282, 1, 1, NULL, NULL, NULL, NULL, 11850, 'rentOut', ''),
('P000000013', 'W000000029', 'E000000003', 'I', '6', 'E', 603, 538, NULL, NULL, NULL, NULL, NULL, NULL, 16000, 'available', ''),
('P000000014', 'W000000023', 'E000000002', 'E', '20', 'E', 484, 378, 2, NULL, NULL, NULL, NULL, 5500000, NULL, 'sold', ''),
('P000000015', 'W000000019', 'E000000002', 'E', '25', 'A', 480, 356, 2, NULL, NULL, NULL, NULL, NULL, 14800, 'rentOut', ''),
('P000000016', 'W000000020', 'E000000004', '', '12', 'A', 430, 346, 2, NULL, NULL, NULL, NULL, 4500000, NULL, 'sold', ''),
('P000000017', 'W000000021', 'E000000002', 'B', '10', 'B', 484, 378, 2, NULL, NULL, NULL, NULL, NULL, 16000, 'rentOut', ''),
('P000000018', 'W000000025', 'E000000002', 'F', '8', 'C', NULL, 390, 2, NULL, NULL, NULL, NULL, NULL, 14000, 'available', ''),
('P000000019', 'W000000024', 'E000000002', 'F', '20', 'C', 462, 390, 2, NULL, NULL, NULL, NULL, 5500000, 14500, 'available', ''),
('P000000020', 'W000000026', 'E000000002', 'H', '11', 'A', 443, 322, 1, NULL, NULL, NULL, NULL, NULL, 12500, 'available', ''),
('P000000021', 'W000000027', 'E000000003', 'C', '22', 'A', 603, 538, 2, NULL, NULL, NULL, NULL, 7100000, NULL, 'available', ''),
('P000000022', 'W000000028', 'E000000001', 'B', '38', 'F', 528, 382, 2, NULL, NULL, NULL, NULL, NULL, 13200, 'available', ''),
('P000000023', 'W000000019', 'E000000002', 'J', '32', 'E', 462, 389, 2, NULL, NULL, NULL, NULL, NULL, 14800, 'available', ''),
('P000000024', 'W000000022', 'E000000004', '', '10', 'B', 478, 403, 2, NULL, NULL, NULL, NULL, NULL, 15000, 'available', ''),
('P000000025', 'W000000008', 'E000000002', 'D', '10', 'B', 344, 265, 1, NULL, NULL, NULL, NULL, 4700000, NULL, 'available', ''),
('P000000026', 'W000000022', 'E000000004', '', '11', 'A', 430, 346, 2, NULL, NULL, NULL, NULL, 4400000, NULL, 'available', ''),
('P000000027', 'W000000011', 'E000000003', 'E', '15', 'E', NULL, 469, 2, NULL, NULL, NULL, NULL, NULL, 15500, 'available', ''),
('P000000028', 'W000000018', 'E000000003', 'D', '16', 'A', 596, 532, 2, NULL, NULL, NULL, NULL, 6480000, NULL, 'available', ''),
('P000000029', 'W000000025', 'E000000003', 'B', '28', 'B', 590, 520, 2, NULL, NULL, NULL, NULL, 6600000, NULL, 'available', ''),
('P000000030', 'W000000014', 'E000000003', 'A', '12', 'A', 604, 538, 3, 2, 2, NULL, NULL, 7380000, NULL, 'available', ''),
('P000000031', 'W000000030', 'E000000003', 'B', '28', 'A', 604, 538, 3, NULL, NULL, NULL, NULL, 7500000, NULL, 'available', ''),
('P000000032', 'W000000031', 'E000000003', 'F', '11', 'B', 590, 517, 3, NULL, NULL, NULL, NULL, 6380000, NULL, 'available', ''),
('P000000033', 'W000000032', 'E000000003', 'F', '15', 'C', 533, 436, 2, NULL, NULL, NULL, NULL, 6000000, NULL, 'available', ''),
('P000000034', 'W000000033', 'E000000003', 'G', '26', 'A', 609, 527, 3, NULL, NULL, NULL, NULL, NULL, 19000, 'available', ''),
('P000000035', 'W000000034', 'E000000003', 'D', '22', 'A', 596, 532, 2, NULL, NULL, NULL, NULL, NULL, 17000, 'available', ''),
('P000000036', 'W000000035', 'E000000001', 'D', '25', 'F', 528, 382, 2, NULL, NULL, NULL, NULL, 5380000, NULL, 'available', ''),
('P000000037', 'W000000036', 'E000000001', 'E', '8', 'C', 343, 252, 2, NULL, NULL, NULL, NULL, 4100000, NULL, 'available', ''),
('P000000038', 'W000000037', 'E000000001', 'A', '7', 'A', 311, 253, 1, NULL, NULL, NULL, NULL, 3950000, 11000, 'available', ''),
('P000000039', 'W000000038', 'E000000001', 'D', '28', 'A', 311, 253, 1, NULL, NULL, 'open', NULL, NULL, 13800, 'available', ''),
('P000000040', 'W000000039', 'E000000001', 'F', '17', 'B', 336, 265, NULL, NULL, NULL, NULL, NULL, 4280000, NULL, 'available', ''),
('P000000041', 'W000000040', 'E000000001', 'D', '32', 'F', 528, 382, NULL, NULL, NULL, NULL, NULL, 5500000, NULL, 'available', ''),
('P000000042', 'W000000008', 'E000000005', '1', 'H', 'B', 1107, 846, 3, 2, 2, NULL, NULL, 16800000, NULL, 'sold', '1 Workers room'),
('P000000043', 'W000000008', 'E000000005', '2', 'M', 'D', 2163, 1689, 4, 2, 3, NULL, NULL, 38000000, NULL, 'available', ''),
('P000000044', 'W000000008', 'E000000005', '2', 'H', 'D', 2163, 1689, 4, 2, 3, NULL, NULL, 42000000, 98000, 'available', NULL),
('P000000045', 'W000000008', 'E000000005', '2', 'L', 'E', 1101, 843, 4, 2, 3, NULL, NULL, 15500000, 35000, 'rentOut', NULL),
('P000000046', 'W000000041', 'E000000005', '1', 'H', 'C', 1623, 1269, 4, 2, 3, NULL, NULL, 37000000, 60000, 'available', NULL),
('P000000047', 'W000000028', 'E000000008', '9', 'M', 'A', 928, 700, 3, 2, NULL, NULL, NULL, 14500000, NULL, 'available', NULL),
('P000000048', 'W000000008', 'E000000009', '1', 'H', 'A', 2068, 1592, 6, 4, NULL, NULL, NULL, 37000000, NULL, 'available', NULL),
('P000000049', 'W000000008', 'E000000009', '1', 'H', 'B', 2555, 1983, 4, 2, NULL, NULL, NULL, 58000000, 90000, 'available', NULL),
('P000000050', 'W000000008', 'E000000009', '1', 'H', 'F', 2115, 1634, 6, 4, NULL, NULL, NULL, 40000000, 100000, 'available', 'Duplex flat, including all appliances'),
('P000000051', 'W000000008', 'E000000009', '2', 'H', 'F', 983, 794, 3, 2, NULL, NULL, NULL, NULL, 35000, 'available', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Staff`
--

CREATE TABLE `Staff` (
  `staff_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `staff_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff_contact_num` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_ID` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Staff`
--

INSERT INTO `Staff` (`staff_ID`, `staff_name`, `staff_contact_num`, `branch_ID`, `staff_type`, `username`, `password`) VALUES
('A000000001', 'Andy Yeung ', '98890001', 'B0001', 'agent', 'staff', 'password'),
('A000000002', 'Brian Chan', '98890002', 'B0001', 'agent', 'brianchan', 'password'),
('A000000003', 'Cathy Lam', '98890003', 'B0001', 'agent', 'cathylam', 'password'),
('A000000004', 'Dennis Ng', '98890004', 'B0002', 'agent', 'dennisng', 'password'),
('A000000005', 'Esther Lee', '98890005', 'B0002', 'agent', 'estherlee', 'password'),
('A000000006', 'Frank Cheung', '98890006', 'B0003', 'agent', 'frankcheung', 'password'),
('A000000007', 'George Ho', '98890007', 'B0003', 'agent', 'georgeho ', 'password'),
('A000000008', 'Anson Leung', '97790009', 'B0003', 'agent', 'ansonleung', 'password'),
('A000000009', 'Iris Wong', '98890009', 'B0003', 'agent', 'iriswong', 'password'),
('A000000010', 'Phoebe Lee', '97669000', 'B0003', 'agent', 'phoebeleung', 'password'),
('A000000011', 'Sam Chan', '98888005', 'B0002', 'agent', 'samchan', 'password'),
('M000000001', 'Alex Cheung', '87878787', 'B0001', 'manager', 'manager', 'password'),
('M000000002', 'Bonnie Chan', '23456789', 'B0002', 'manager', 'bonniechan', 'password'),
('M000000003', 'Connie Wong', '98765432', 'B0003', 'manager', 'conniewong', 'password'),
('X000000001', 'Super Admin', '', NULL, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `SubDistrict`
--

CREATE TABLE `SubDistrict` (
  `sub_district_ID` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sub_district_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district_ID` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SubDistrict`
--

INSERT INTO `SubDistrict` (`sub_district_ID`, `sub_district_name`, `district_ID`) VALUES
('SD001', 'Kennedy Town', 'D0001'),
('SD002', 'Shek Tong Tsui', 'D0001'),
('SD003', 'Sai Ying Pun', 'D0001'),
('SD004', 'Sheung Wan', 'D0001'),
('SD005', 'Central', 'D0001'),
('SD006', 'Admiralty', 'D0001'),
('SD007', 'Mid-levels', 'D0001'),
('SD008', 'Peak', 'D0001'),
('SD009', 'Wan Chai', 'D0002'),
('SD010', 'Causeway Bay', 'D0002'),
('SD011', 'Happy Valley', 'D0002'),
('SD012', 'Tai Hang', 'D0002'),
('SD013', 'So Kon Po', 'D0002'),
('SD014', 'Jardine''s Lookout', 'D0002'),
('SD015', 'Tin Hau', 'D0003'),
('SD016', 'Braemar Hill', 'D0003'),
('SD017', 'North Point', 'D0003'),
('SD018', 'Quarry Bay', 'D0003'),
('SD019', 'Sai Wan Ho', 'D0003'),
('SD020', 'Shau Kei Wan', 'D0003'),
('SD021', 'Chai Wan', 'D0003'),
('SD022', 'Siu Sai Wan', 'D0003'),
('SD023', 'Pok Fu Lam', 'D0004'),
('SD024', 'Aberdeen', 'D0004'),
('SD025', 'Ap Lei Chau', 'D0004'),
('SD026', 'Wong Chuk Hang', 'D0004'),
('SD027', 'Shouson Hill', 'D0004'),
('SD028', 'Repulse Bay', 'D0004'),
('SD029', 'Chung Hom Kok', 'D0004'),
('SD030', 'Stanley', 'D0004'),
('SD031', 'Tai Tam', 'D0004'),
('SD032', 'Shek O', 'D0004'),
('SD033', 'Tsim Sha Tsui', 'D0005'),
('SD034', 'Yau Ma Tei', 'D0005'),
('SD035', 'West Kowloon Reclamation', 'D0005'),
('SD036', 'King''s Park', 'D0005'),
('SD037', 'Mong Kok', 'D0005'),
('SD038', 'Tai Kok Tsui', 'D0005'),
('SD039', 'Mei Foo', 'D0006'),
('SD040', 'Lai Chi Kok', 'D0006'),
('SD041', 'Cheung Sha Wan', 'D0006'),
('SD042', 'Sham Shui Po', 'D0006'),
('SD043', 'Shek Kip Mei', 'D0006'),
('SD044', 'Yau Yat Tsuen', 'D0006'),
('SD045', 'Tai Wo Ping', 'D0006'),
('SD046', 'Stonecutters Island', 'D0006'),
('SD047', 'Hung Hom', 'D0007'),
('SD048', 'To Kwa Wan', 'D0007'),
('SD049', 'Ma Tau Kok', 'D0007'),
('SD050', 'Ma Tau Wai', 'D0007'),
('SD051', 'Kai Tak', 'D0007'),
('SD052', 'Kowloon City', 'D0007'),
('SD053', 'Ho Man Tin', 'D0007'),
('SD054', 'Kowloon Tong', 'D0007'),
('SD055', 'Beacon Hill', 'D0007'),
('SD056', 'San Po Kong', 'D0008'),
('SD057', 'Wong Tai Sin', 'D0008'),
('SD058', 'Tung Tau', 'D0008'),
('SD059', 'Wang Tau Hom', 'D0008'),
('SD060', 'Lok Fu', 'D0008'),
('SD061', 'Diamond Hill', 'D0008'),
('SD062', 'Tsz Wan Shan', 'D0008'),
('SD063', 'Ngau Chi Wan', 'D0008'),
('SD064', 'Ping Shek', 'D0009'),
('SD065', 'Kowloon Bay', 'D0009'),
('SD066', 'Ngau Tau Kok', 'D0009'),
('SD067', 'Jordan Valley', 'D0009'),
('SD068', 'Kwun Tong', 'D0009'),
('SD069', 'Sau Mau Ping', 'D0009'),
('SD070', 'Lam Tin', 'D0009'),
('SD071', 'Yau Tong', 'D0009'),
('SD072', 'Lei Yue Mun', 'D0009'),
('SD073', 'Kwai Chung', 'D0010'),
('SD074', 'Tsing Yi', 'D0010'),
('SD075', 'Tsuen Wan', 'D0011'),
('SD076', 'Lei Muk Shue', 'D0011'),
('SD077', 'Ting Kau', 'D0011'),
('SD078', 'Sham Tseng', 'D0011'),
('SD079', 'Tsing Lung Tau', 'D0011'),
('SD080', 'Ma Wan', 'D0011'),
('SD081', 'Sunny Bay', 'D0011'),
('SD082', 'Tai Lam Chung', 'D0012'),
('SD083', 'So Kwun Wat', 'D0012'),
('SD084', 'Tuen Mun', 'D0012'),
('SD085', 'Lam Tei', 'D0012'),
('SD086', 'Hung Shui Kiu', 'D0013'),
('SD087', 'Ha Tsuen', 'D0013'),
('SD088', 'Lau Fau Shan', 'D0013'),
('SD089', 'Tin Shui Wai', 'D0013'),
('SD090', 'Yuen Long', 'D0013'),
('SD091', 'San Tin', 'D0013'),
('SD092', 'Lok Ma Chau', 'D0013'),
('SD093', 'Kam Tin', 'D0013'),
('SD094', 'Shek Kong', 'D0013'),
('SD095', 'Pat Heung', 'D0013'),
('SD096', 'Fanling', 'D0014'),
('SD097', 'Luen Wo Hui', 'D0014'),
('SD098', 'Sheung Shui', 'D0014'),
('SD099', 'Shek Wu Hui', 'D0014'),
('SD100', 'Sha Tau Kok', 'D0014'),
('SD101', 'Luk Keng', 'D0014'),
('SD102', 'Wu Kau Tang', 'D0014'),
('SD103', 'Tai Po Market', 'D0015'),
('SD104', 'Tai Po', 'D0015'),
('SD105', 'Tai Po Kau', 'D0015'),
('SD106', 'Tai Mei Tuk', 'D0015'),
('SD107', 'Shuen Wan', 'D0015'),
('SD108', 'Cheung Muk Tau', 'D0015'),
('SD109', 'Kei Ling Ha', 'D0015'),
('SD110', 'Tai Wai', 'D0016'),
('SD111', 'Sha Tin', 'D0016'),
('SD112', 'Fo Tan', 'D0016'),
('SD113', 'Ma Liu Shui', 'D0016'),
('SD114', 'Wu Kai Sha', 'D0016'),
('SD115', 'Ma On Shan', 'D0016'),
('SD116', 'Clear Water Bay', 'D0017'),
('SD117', 'Sai Kung', 'D0017'),
('SD118', 'Tai Mong Tsai', 'D0017'),
('SD119', 'Tseung Kwan O', 'D0017'),
('SD120', 'Hang Hau', 'D0017'),
('SD121', 'Tiu Keng Leng', 'D0017'),
('SD122', 'Ma Yau Tong', 'D0017'),
('SD123', 'Cheung Chau', 'D0018'),
('SD124', 'Peng Chau', 'D0018'),
('SD125', 'Lantau Island', 'D0018'),
('SD126', 'Lamma Island', 'D0018');

-- --------------------------------------------------------

--
-- Table structure for table `Transaction`
--

CREATE TABLE `Transaction` (
  `transaction_ref` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `transaction_date` date DEFAULT NULL,
  `transaction_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_price` int(10) DEFAULT NULL,
  `commission` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Transaction`
--

INSERT INTO `Transaction` (`transaction_ref`, `transaction_date`, `transaction_type`, `property_ID`, `cust_ID`, `agent_ID`, `owner_ID`, `transaction_price`, `commission`) VALUES
('T000000001', '2017-09-01', 'sell', 'P000000002', 'C000000002', 'A000000002', 'W000000038', 4000000, 80000),
('T000000002', '2017-09-01', 'sell', 'P000000003', 'C000000004', 'A000000001', 'W000000005', 5300000, 106000),
('T000000003', '2017-09-02', 'sell', 'P000000001', 'C000000001', 'A000000001', 'W000000001', 3000000, 60000),
('T000000004', '2017-09-03', 'rent', 'P000000004', 'C000000006', 'A000000002', 'W000000003', 12000, 12000),
('T000000005', '2017-09-04', 'sell', 'P000000005', 'C000000001', 'A000000001', 'W000000002', 4800000, 96000),
('T000000006', '2017-09-04', 'sell', 'P000000006', 'C000000006', 'A000000001', 'W000000004', 5200000, 104000),
('T000000007', '2017-09-06', 'sell', 'P000000007', 'C000000007', 'A000000003', 'W000000007', 5000000, 100000),
('T000000008', '2017-09-07', 'sell', 'P000000008', 'C000000001', 'A000000002', 'W000000001', 4100000, 82000),
('T000000009', '2017-09-08', 'rent', 'P000000008', 'C000000005', 'A000000003', 'W000000008', 12000, 12000),
('T000000010', '2017-09-09', 'sell', 'P000000009', 'C000000009', 'A000000002', 'W000000010', 4900000, 98000),
('T000000011', '2017-09-10', 'rent', 'P000000010', 'C000000011', 'A000000001', 'W000000012', 16000, 16000),
('T000000012', '2017-09-11', 'sell', 'P000000011', 'C000000013', 'A000000001', 'W000000008', 7380000, 147600),
('T000000013', '2017-09-12', 'rent', 'P000000012', 'C000000012', 'A000000003', 'W000000009', 11850, 11850),
('T000000014', '2017-09-13', 'sell', 'P000000013', 'C000000017', 'A000000003', 'W000000015', 6780000, 135600),
('T000000015', '2017-09-14', 'sell', 'P000000014', 'C000000018', 'A000000003', 'W000000023', 5500000, 110000),
('T000000016', '2017-09-15', 'rent', 'P000000015', 'C000000019', 'A000000002', 'W000000019', 14800, 14800),
('T000000017', '2017-09-16', 'sell', 'P000000016', 'C000000020', 'A000000001', 'W000000020', 4500000, 90000),
('T000000018', '2017-09-17', 'rent', 'P000000017', 'C000000015', 'A000000001', 'W000000021', 16000, 16000),
('T000000019', '2017-10-17', 'sell', 'P000000042', 'C000000020', 'A000000009', 'W000000008', 16800000, 336000),
('T000000020', '2017-10-20', 'rent', 'P000000045', 'C000000019', 'A000000005', 'W000000008', 35000, 35000),
('T000000021', '2017-10-20', 'sell', 'P000000050', 'C000000001', 'A000000006', 'W000000003', 35000000, 700000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Branch`
--
ALTER TABLE `Branch`
  ADD PRIMARY KEY (`branch_ID`);

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`cust_ID`),
  ADD KEY `pref_district_ID` (`pref_district_ID`),
  ADD KEY `pref_sub_district_ID` (`pref_sub_district_ID`),
  ADD KEY `pref_estate_ID` (`pref_estate_ID`);

--
-- Indexes for table `District`
--
ALTER TABLE `District`
  ADD PRIMARY KEY (`district_ID`);

--
-- Indexes for table `Estate`
--
ALTER TABLE `Estate`
  ADD PRIMARY KEY (`estate_ID`),
  ADD KEY `sub_district_ID` (`sub_district_ID`);

--
-- Indexes for table `Owner`
--
ALTER TABLE `Owner`
  ADD PRIMARY KEY (`owner_ID`);

--
-- Indexes for table `Property`
--
ALTER TABLE `Property`
  ADD PRIMARY KEY (`property_ID`),
  ADD KEY `owner_ID` (`owner_ID`),
  ADD KEY `estate_ID` (`estate_ID`);

--
-- Indexes for table `Staff`
--
ALTER TABLE `Staff`
  ADD PRIMARY KEY (`staff_ID`),
  ADD KEY `branch_ID` (`branch_ID`);

--
-- Indexes for table `SubDistrict`
--
ALTER TABLE `SubDistrict`
  ADD PRIMARY KEY (`sub_district_ID`),
  ADD KEY `district_ID` (`district_ID`);

--
-- Indexes for table `Transaction`
--
ALTER TABLE `Transaction`
  ADD PRIMARY KEY (`transaction_ref`),
  ADD KEY `property_ID` (`property_ID`),
  ADD KEY `cust_ID` (`cust_ID`),
  ADD KEY `agent_ID` (`agent_ID`),
  ADD KEY `owner_ID` (`owner_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
