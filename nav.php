<?php
session_start();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark-brown fixed-top">
  <div class="container">
    <a class="navbar-brand" href="/ADD-property/">The HK Property</a>
    <div class="nav-login">
      <a class="btn-nav btn-about" href="#" data-toggle="modal" data-target="#aboutModal">About</a>
      <?php
      if (isset($_SESSION["username"])) {
        ?>
      <a href="#" class="btn-nav btn-logout" id="btn-logout">[ Logut ]</a>
        <span>Welcome <em><?php echo $_SESSION["staffType"] ?></em>, <?php echo $_SESSION["staffName"] ?></span> 
      <?php } else {?>
        <a  class="btn-nav" href="#" data-toggle="modal" data-target="#loginModal">Staff Login</a>

      <?php } ?>
    </div>
  </div>
</nav>

<!-- Modal -->
<div class="modal fade" id="aboutModal" tabindex="-1" role="dialog" aria-labelledby="aboutModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="aboutModalLabel">About</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        This is the Live Demo for ADD Assessment by
        <div class="row">
          <div class="col-sm-12 col-md-3">Student: </div>
          <div class="col-sm-12 col-md-9">ON Sin Yu (Oscar)</div>
          <div class="col-sm-12 col-md-3">Group: </div>
          <div class="col-sm-12 col-md-9">G4</div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Staff login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <div class="login-error-msg" id="login-error"><span></span></div>
        <div class="form-group">
          <label for="isername">Username</label>
          <input type="text" class="form-control" id="username" placeholder="">

        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" placeholder="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-login">Login</button>
      </div>

    </div>
  </div>
</div>
