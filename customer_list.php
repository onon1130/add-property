<?php include 'DB_connect.php'; ?>
<?php
$cust_id = $_REQUEST["cust_id"];
$cust_name = $_REQUEST["cust_name"];
$cust_contact_num = $_REQUEST["cust_contact_num"];
echo "<!-- cust_id :" . $cust_id . "<br />-->";
echo "<!-- cust_name  :" . $cust_name . "<br />-->";
echo "<!-- cust_contact_num :" . cust_contact_num . "<br />-->";
$sql = "SELECT"
        . " CS.*,"
        . " ES.estate_name,"
        . " SD.sub_district_name,"
        . " DT.district_name"
        . " FROM Customer as CS"
        . " LEFT JOIN Estate as ES on CS.pref_estate_ID = ES.estate_ID"
        . " LEFT JOIN SubDistrict as SD on CS.pref_sub_district_ID = SD.sub_district_id"
        . " LEFT JOIN District as DT on CS.pref_district_ID = DT.district_ID";
if (!empty($cust_id) || !empty($cust_name) || !empty($cust_contact_num)) {
  $sql = $sql . " WHERE";
}
if (!empty($cust_id)) {
 $sql = $sql . " CS.cust_ID LIKE '%$cust_id%'";
 if ( !empty($cust_name) || !empty($cust_contact_num)) {
   $sql = $sql . " AND";
 };
}
if (!empty($cust_name)) {
 $sql = $sql . " CS.cust_name LIKE '%$cust_name%'";
 if (!empty($cust_contact_num)) {
   $sql = $sql . " AND";
 };
}
if (!empty($cust_contact_num)) {
 $sql = $sql . " CS.cust_contact_num LIKE '%$cust_contact_num%'";
}

echo "<!-- SQL :" . $sql . "-->";
$result = $conn->query($sql);
$resultCount = $result->num_rows;
?>
<!DOCTYPE html>
<html lang="en">
  <?php include 'head.php'; ?>
  <body>
    <!-- Navigation -->
    <?php include 'nav.php'; ?>
    <!-- Header - set the background image for the header in the line below -->
    <header class="bg-image-full header-tab header-tab-page">
      <div class="container">
        <div class="header-wrapper">
          <nav aria-label="breadcrumb" role="navigation" class="page-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/ADD-property/">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">
                Search Customer with 
                <?php if ($cust_id) { echo ' ID  "<strong>'. $cust_id .'</strong>"';}?>
                <?php if ($cust_name) { echo ' name  "<strong>'. $cust_name .'</strong>"';}?>
                <?php if ($cust_contact_num) { echo ' contact number  "<strong>'. $cust_contact_num .'</strong>"';}?>   
            </ol>
          </nav>
        </div>
      </div>
    </header>
    <div class="container">
      <?php
      if ($resultCount > 0) {
        $rowNum = 0;
      ?>
        <table class="table table-hover table-property">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Customer Id</th>
              <th scope="col">Customer Name</th>
              <th scope="col">Contact Number</th>
              <th scope="col">Preferred District</th>
              <th scope="col">Preferred Estate</th>
              <th scope="col">Budget for buy</th>
              <th scope="col">Budget for rent</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <?php
            while ($row = $result->fetch_assoc()) {
              $rowNum += 1;
              ?>
              <tr>
                <th scope="row"><?php echo $rowNum ?></th>
                <td nowrap><?php echo $row["cust_ID"] ?></td>
                <td><?php echo $row["cust_name"] ?></td>
                <td nowrap><?php echo $row["cust_contact_num"] ?></td>
                <td><?php if (isset($row["district_name"]) && trim($row["district_name"])) echo $row["district_name"];
                  if (isset($row["district_name"]) && trim($row["district_name"]) && isset($row["sub_district_name"]) && trim($row["sub_district_name"])){ echo ", "; } else { echo "--";} 
                  if (isset($row["sub_district_name"]) && trim($row["sub_district_name"]))  echo $row["sub_district_name"] ?>  
                </td>
                <td>
                  <?php if (!isset($row["estate_name"]) || trim($row["estate_name"]) === '') {
                  echo "--";
                  } else {
                    echo $row["estate_name"];
                  } ?>
                </td>
               <td>
                 
                <?php if (!isset($row["budget_buy"]) || trim($row["budget_buy"]) === '' || $row["budget_buy"] === 0) {
                  echo " --";
                } else { ?>
                <a href="#" class="viewProperty" data-cust-id="<?php echo $row["cust_ID"] ?>" data-property-type="sell">
                  <?php echo  number_format($row["budget_buy"])?>
                </a>
                <?php } ?>
                 </td>
               <td>
                <?php if (!isset($row["budget_rent"]) || trim($row["budget_rent"]) === '' || $row["budget_rent"] === 0) {
                  echo "--";
                } else { ?>
               <a href="#" class="viewProperty" data-cust-id="<?php echo $row["cust_ID"] ?>" data-property-type="rent">
                  <?php echo  number_format($row["budget_rent"])?>
                </a>
                   <?php }?>
                </td>
                <td>
                <?php
                if ((!isset($row["budget_buy"]) || trim($row["budget_buy"]) === '' || $row["budget_buy"] === 0) &&(!isset($row["budget_rent"]) || trim($row["budget_rent"]) === '' || $row["budget_rent"] === 0)) {
                  ?> 
                
                <a href="#" class="viewProperty" data-cust-id="<?php echo $row["cust_ID"] ?>" data-property-type="">
                  Any
                </a>
                
                <?php } ?>
                 </td>
                 
                 
                </td>
              </tr>
          <?php } ?>
          </tbody>
        </table>
          <?php
        } else {
          echo "<div class='noRecord-box'>Sorry, No records found.</div>";
        }
        $conn->close();
        ?>
    </div>
    <!-- Footer -->
    <?php include 'footer.php'; ?>
  </body>
 
</html>
