<?php include 'DB_connect.php'; ?>
<?php
$property_id = $_REQUEST["property_id"];
echo "<!-- property_id :" . $property_id . "<br />-->";
$sql = "SELECT"
        . " PT.*,"
        . " OW.owner_name,"
        . " OW.owner_contact_num,"
        . " ES.estate_name,"
        . " SD.sub_district_name,"
        . " DT.district_name"
        . " FROM District as DT"
        . " INNER JOIN SubDistrict as SD on SD.district_ID = DT.district_ID"
        . " INNER JOIN Estate as ES on ES.sub_district_ID = SD.sub_district_ID"
        . " INNER JOIN Property as PT on PT.estate_ID = ES.estate_ID"
        . " INNER JOIN Owner as OW on OW.owner_ID = PT.owner_ID"
        . " WHERE PT.property_ID ='$property_id'";
echo "<!-- SQL :" . $sql . "-->";
$result = $conn->query($sql);
$resultCount = $result->num_rows;
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'; ?>
  <body>
    <!-- Navigation -->
    <?php include 'nav.php'; ?>
    <!-- Header - set the background image for the header in the line below -->
    <header class="bg-image-full header-tab header-tab-page">
      <div class="container">
        <div class="header-wrapper">
          <nav aria-label="breadcrumb" role="navigation" class="page-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/ADD-property/">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">
                Property with ID <strong><?php echo $property_id; ?></strong>   
            </ol>
          </nav>
        </div>
      </div>
    </header>
    <div class="container">
       <?php
      if ($resultCount > 0) {
        while ($row = $result->fetch_assoc()) {
              ?>  
          <table class="table  table-property" style="border: 1px solid #dae0e5">
            <thead>
              <tr>

                <th scope="col">Property ID</th>
                <th scope="col">District</th>
                <th scope="col">Estate</th>
                <th scope="col"></th>
                <th scope="col">Floor Area</th>
                <th scope="col">Rooms & others</th>
                <th scope="col">Selling $</th>
                <th scope="col">Rental $</th>
                <th scope="col">Remark</th>
              </tr>
            </thead>
            <tbody>            
              <tr>
                <td nowrap><?php echo $row["property_ID"] ?>
                  </td>
                <td><?php echo $row["district_name"] ?>,  <?php echo $row["sub_district_name"] ?></td>
                <td><?php echo $row["estate_name"] ?></td>
                <td nowrap>
                  <span>Block </span><?php if (!isset($row["block_num"]) || trim($row["block_num"]) === '') {
                  echo "--";
                  } else {
                    echo $row["block_num"];
                  } ?><br />
                          <span> Floor </span><?php if (!isset($row["floor_num"]) || trim($row["floor_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["floor_num"];
                  } ?><br />
                          <span>Flat </span><?php if (!isset($row["flat_num"]) || trim($row["flat_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["flat_num"];
                  } ?></td>
                        <td nowrap>
                          <span>Gross:</span> <?php if (!isset($row["gross_area"]) || trim($row["gross_area"]) === '' || $row["gross_area"] === 0) {
                    echo "--";
                  } else {
                    echo $row["gross_area"] . "<span> sqft</span>";
                  } ?><br/>
                          <span>Net: </span><?php if (!isset($row["net_area"]) || trim($row["net_area"]) === '' || $row["net_area"] === 0) {
                    echo "--";
                  } else {
                    echo $row["net_area"] . "<span> sqft</span>";
                  } ?>
                </td>
                <td nowrap>
                  <span>No. of living rooms: </span><?php if (!isset($row["livingroom_num"]) || trim($row["livingroom_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["livingroom_num"];
                  } ?><br/>
                  <span>No. of bedrooms: </span><?php if (!isset($row["bedroom_num"]) || trim($row["bedroom_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["bedroom_num"];
                  } ?><br/>
                              <span>No. of bathrooms: </span><?php if (!isset($row["bathroom_num"]) || trim($row["bathroom_num"]) === '') {
                    echo "--";
                  } else {
                    echo $row["bathroom_num"];
                  } ?><br/>
                              <span>Kitchen style: </span><?php if (!isset($row["kitchen"]) || trim($row["kitchen"]) === '') {
                    echo "--";
                  } else {
                    echo $row["kitchen"];
                  } ?><br/>
                              <span>Car Park provided: </span><?php if (!isset($row["carpark"]) || trim($row["carpark"]) === '') {
                    echo "No";
                  } else {
                    echo "Yes";
                  } ?>
                </td>
                <td class="td-price">
                <?php if (!isset($row["selling_price"]) || trim($row["selling_price"]) === '' || $row["selling_price"] === 0) {
                  echo "--";
                } else {
                  echo "<span> $</span>" . number_format($row["selling_price"]);
                } ?>
                <?php if (trim($row["status"]) === 'sold') {
                  echo "<br /><span class='statusAlert'> Sold</span>";
                } ?>
               </td>
               <td class="td-price">
                <?php if (!isset($row["rental_price"]) || trim($row["rental_price"]) === '' || $row["rental_price"] === 0) {
                  echo "--";
                } else {
                  echo "<span> $</span>" . number_format($row["rental_price"]);
                } ?>
                <?php if (trim($row["status"]) === 'rentOut') {
                  echo "<br /><span class='statusAlert'> Rent out</span>";
                } ?>
                </td>
                <td><span><?php if (!isset($row["remarks"]) || trim($row["remarks"]) === '' || $row["remarks"] === 0) {
                    echo "--";
                  } else {
                    echo $row["remarks"];
                  } ?></span>
                </td>
              </tr>
          </tbody>
        </table>
          <?php if (isset($_SESSION["username"])) { ?>
          <!-- for Agents / managers or admin only start -->
          <div class="customerInfo">
            <h6> Owner</h6>
            <div class="row">
              <div class="col-sm-12 col-md-4">
                  <div class="col-sm-3"><span>ID: </span></div>
                  <div class="col-sm-9"><?php echo $row["owner_ID"]?></div>
                  <div class="col-sm-3"><span> Name:</span></div>
                  <div class="col-sm-9"><?php echo $row["owner_name"] ?></div>
                  <div class="col-sm-3"><span>Contact:</span></div>
                  <div class="col-sm-9"> <?php echo $row["owner_contact_num"] ?></div>
              </div>
            </div>
          </div>
          <?php
          $ownerID=$row["owner_ID"];
          $sqlOwnerCurrent = "SELECT" 
            . " PT.*,"
            . " ES.estate_name,"
            . " SD.sub_district_name,"
            . " DT.district_name"
            . " FROM District as DT"
            . " INNER JOIN SubDistrict as SD on SD.district_ID = DT.district_ID"
            . " INNER JOIN Estate as ES on ES.sub_district_ID = SD.sub_district_ID"
            . " INNER JOIN Property as PT on PT.estate_ID = ES.estate_ID"
            . " WHERE PT.owner_ID ='$ownerID'"
            . " AND PT.property_ID <>'$property_id'";
              echo "<!-- SQL :" . $sqlOwnerCurrent . "-->";
              $resultOwner = $conn->query($sqlOwnerCurrent);
              $resultCountOwner = $resultOwner->num_rows;
              $rowNum = 0;
              if ($resultCountOwner > 0) {
                ?>
                <br /><h8> Other property(s) of this owner :</h8>
                <table class="table table-hover table-property">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Property ID</th>
                      <th scope="col">District</th>
                      <th scope="col">Estate</th>
                      <th scope="col"></th>
                      <th scope="col">Floor Area</th>
                      <th scope="col">Rooms & others</th>
                      <th scope="col">Selling $</th>
                      <th scope="col">Rental $</th>
                      <th scope="col">Remark</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                     while ($rowOwner = $resultOwner->fetch_assoc()) {
                      $rowNum += 1;
                      ?>
                      <tr>
                        <th scope="row"><?php echo $rowNum ?></th>
                        <td nowrap><?php echo $rowOwner["property_ID"] ?></td>  
                        <td><?php echo $rowOwner["district_name"] ?>,  <?php echo $rowOwner["sub_district_name"] ?></td>
                        <td><?php echo $rowOwner["estate_name"] ?></td>
                        <td nowrap>
                          <span>Block </span><?php if (!isset($rowOwner["block_num"]) || trim($rowOwner["block_num"]) === '') {
                          echo "--";
                          } else {
                            echo $rowOwner["block_num"];
                          } ?><br />
                                  <span> Floor </span><?php if (!isset($rowOwner["floor_num"]) || trim($rowOwner["floor_num"]) === '') {
                            echo "--";
                          } else {
                            echo $rowOwner["floor_num"];
                          } ?><br />
                                  <span>Flat </span><?php if (!isset($rowOwner["flat_num"]) || trim($rowOwner["flat_num"]) === '') {
                            echo "--";
                          } else {
                            echo $rowOwner["flat_num"];
                          } ?></td>
                                <td nowrap>
                                  <span>Gross:</span> <?php if (!isset($rowOwner["gross_area"]) || trim($rowOwner["gross_area"]) === '' || $rowOwner["gross_area"] === 0) {
                            echo "--";
                          } else {
                            echo $rowOwner["gross_area"] . "<span> sqft</span>";
                          } ?><br/>
                                  <span>Net: </span><?php if (!isset($rowOwner["net_area"]) || trim($rowOwner["net_area"]) === '' || $rowOwner["net_area"] === 0) {
                            echo "--";
                          } else {
                            echo $rowOwner["net_area"] . "<span> sqft</span>";
                          } ?>
                        </td>
                        <td nowrap>
                          <span>No. of living rooms: </span><?php if (!isset($rowOwner["livingroom_num"]) || trim($rowOwner["livingroom_num"]) === '') {
                            echo "--";
                          } else {
                            echo $rowOwner["livingroom_num"];
                          } ?><br/>
                          <span>No. of bedrooms: </span><?php if (!isset($rowOwner["bedroom_num"]) || trim($rowOwner["bedroom_num"]) === '') {
                            echo "--";
                          } else {
                            echo $rowOwner["bedroom_num"];
                          } ?><br/>
                                      <span>No. of bathrooms: </span><?php if (!isset($rowOwner["bathroom_num"]) || trim($rowOwner["bathroom_num"]) === '') {
                            echo "--";
                          } else {
                            echo $rowOwner["bathroom_num"];
                          } ?><br/>
                                      <span>Kitchen style: </span><?php if (!isset($rowOwner["kitchen"]) || trim($rowOwner["kitchen"]) === '') {
                            echo "--";
                          } else {
                            echo $rowOwner["kitchen"];
                          } ?><br/>
                                      <span>Car Park provided: </span><?php if (!isset($rowOwner["carpark"]) || trim($rowOwner["carpark"]) === '') {
                            echo "No";
                          } else {
                            echo "Yes";
                          } ?>
                        </td>
                        <td>
                        <?php if (!isset($rowOwner["selling_price"]) || trim($rowOwner["selling_price"]) === '' || $rowOwner["selling_price"] === 0) {
                          echo "--";
                        } else {
                          echo "<span> $</span>" . number_format($rowOwner["selling_price"]);
                        } ?>
                        <?php if (trim($rowOwner["status"]) === 'sold') {
                          echo "<br /><span class='statusAlert'> Sold</span>";
                        } ?>
                        </td>
                        <td>
                        <?php if (!isset($rowOwner["rental_price"]) || trim($rowOwner["rental_price"]) === '' || $rowOwner["rental_price"] === 0) {
                          echo "--";
                        } else {
                          echo "<span> $</span>" . number_format($rowOwner["rental_price"]);
                        } ?>
                        <?php if (trim($rowOwner["status"]) === 'rentOut') {
                          echo "<br /><span class='statusAlert'> Rent out</span>";
                        } ?>
                        </td>
                        <td><span><?php if (!isset($rowOwner["remarks"]) || trim($rowOwner["remarks"]) === '' || $rowOwner["remarks"] === 0) {
                            echo "--";
                          } else {
                            echo $rowOwner["remarks"];
                          } ?></span>
                        </td>
                      </tr>
                  <?php } ?>
                  </tbody>
                </table>
            <?php } 
        } ?>
        <!-- for Agents / managers or admin only end -->
         <?php } } else {
          echo "<div class='noRecord-box'>Sorry, No records found.</div>";
        }
        $conn->close();
        ?>
    </div>
    <!-- Footer -->
    <?php include 'footer.php'; ?>
  </body>
</html>
