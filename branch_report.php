<?php include 'DB_connect.php'; ?>
<?php
$branch_id = $_REQUEST["branch_id"];
$branch_name = $_REQUEST["branch_name"];
$reportPeriod = $_REQUEST["reportPeriod"];
echo "<!-- branch_id :" . $branch_id . "<br />-->";
echo "<!-- branch_name :" . $branch_name . "<br />-->";
echo "<!-- reportPeriod :" . $reportPeriod . "<br />-->";
$sql = "SELECT"
        . " BH.branch_ID,"
        . " BH.branch_name,"
        . " SM.staff_name as branch_manager,"
        . " SF.staff_ID as SF_agent_ID,"
        . " SF.staff_name as agent_name,"
        . " SUM(CASE WHEN TX.transaction_type = 'rent' THEN 1 ELSE 0 END) AS total_rent_count,"
        . " SUM(CASE WHEN TX.transaction_type = 'rent' THEN TX.transaction_price ELSE NULL END) AS total_rent_price,"
        . " SUM(CASE WHEN TX.transaction_type = 'rent' THEN TX.commission ELSE NULL END) AS total_rent_comm,"
        . " SUM(CASE WHEN TX.transaction_type = 'sell' THEN 1 ELSE 0 END) AS total_sell_count,"
        . " SUM(CASE WHEN TX.transaction_type = 'sell' THEN TX.transaction_price ELSE NULL END) AS total_sell_price,"
        . " SUM(CASE WHEN TX.transaction_type = 'sell' THEN TX.commission ELSE NULL END) AS total_sell_comm,"
        . " COUNT(TX.transaction_price) AS total_TX_count,"
        . " SUM(TX.transaction_price) AS total_TX_price,"
        . " SUM(TX.commission) AS total_TX_comm"
        . " FROM Branch as BH"
        . " INNER JOIN Staff as SF on SF.branch_ID = BH.branch_ID AND SF.staff_type ='agent'"
        . " INNER JOIN Staff as SM on SM.branch_ID = BH.branch_ID AND SM.staff_type ='manager'"
        . " LEFT JOIN Transaction as TX on SF.staff_ID = TX.agent_ID";
        

if (!empty($branch_id)) {
  $sql = $sql . " WHERE SF.branch_id = '$branch_id'";
}
if (!empty($reportPeriod)) {
  $sql = $sql . " AND MONTH(TX.transaction_date) = MONTH(CURRENT_DATE())"
          . " AND YEAR(TX.transaction_date) = YEAR(CURRENT_DATE())";
}


$sql = $sql . " GROUP BY SF_agent_ID, SF.branch_ID";
$sql = $sql . " ORDER BY SF.branch_ID, total_TX_comm DESC,  SF_agent_ID ";

echo "<!-- SQL :" . $sql . "-->";
$result = $conn->query($sql);
$resultCount = $result->num_rows;
?>
<!DOCTYPE html>
<html lang="en">
  <?php include 'head.php'; ?>
  <body>
    <!-- Navigation -->
    <?php include 'nav.php'; ?>
    <!-- Header - set the background image for the header in the line below -->
    <header class="bg-image-full header-tab header-tab-page">
      <div class="container">
        <div class="header-wrapper">
          <nav aria-label="breadcrumb" role="navigation" class="page-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/ADD-property/">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">
                Report of Transaction for 
                <?php if ($branch_id) {
                  echo '<strong>' . $branch_name . '</strong>';
                } else {
                  echo '<strong> All Branches</strong>';
                } ?>   
                on <?php if (!empty($reportPeriod)) {
                  echo '<strong> Current Month</strong>';
                } else {
                  echo '<strong> All Transactions</strong>';
                } ?>
            </ol>
          </nav>
        </div>
      </div>
    </header>
    <div class="container">
      <?php
      if ($resultCount > 0) {
        $rowNum = 0;
        $preBranchID = "default";
        $preTxType = "default";
         $branch_total_tx = 0;
        $branch_total_comm = 0;
        ?>

  <?php
  while ($row = $result->fetch_assoc()) {
    $rowNum += 1;
    $recordNum += 1;
    if ($preBranchID <> $row["branch_ID"]) {
      if ($preBranchID <> "default" ) {
        $rowNum = 1;
        
      ?>
    <tr class="row-total"> 
        <td class="table-subtotal-title" colspan="2">Branch Total : </td>
        <td class="td-price table-subtotal-rent"><?php echo $branch_rent_count ?></td>
        <td class="td-price table-subtotal-rent">$<?php echo number_format($branch_rent_total) ?></td>
        <td class="td-price table-subtotal-rent">$<?php echo number_format($branch_rent_comm) ?></td>
        <td class="td-price table-subtotal-sell"><?php echo $branch_sell_count ?></td>
        <td class="td-price table-subtotal-sell">$<?php echo number_format($branch_sell_total) ?></td>
        <td class="td-price table-subtotal-sell">$<?php echo number_format($branch_sell_comm) ?></td>
        <td class="td-price table-total"><?php echo $branch_total_count ?></td>
        <td class="td-price table-total">$<?php echo number_format($branch_total_tx) ?></td>
        <td class="td-price table-total">$<?php echo number_format($branch_total_comm) ?></td>
    </tr>
    </tbody>
    </table>
</div>
      <?php }
       $branch_rent_count = 0;
       $branch_rent_total = 0;
       $branch_rent_comm = 0;
       $branch_sell_count = 0;
       $branch_sell_total = 0;
       $branch_sell_comm = 0;
       $branch_total_count = 0;
        $branch_total_tx = 0;
        $branch_total_comm = 0;
        $preBranchID = $row["branch_ID"];
      ?>
    <div class="report-wrapper">
      <div class="customerInfo">
        <h6><strong> <?php echo $row["branch_name"] ?> </strong> </h6>
         <span>Manager:<?php echo $row["branch_manager"] ?></span> 
      </div>
      <table class="table table-property">
        <thead>
          <tr>
            <th scope="col" rowspan="2">#</th>
            <th scope="col" rowspan="2">Staff</th>
            <th scope="col" colspan="3" class="tableHeadFirst rowTotalHead-rent">Transaction of Rental</th>
            <th scope="col" colspan="3" class="tableHeadFirst rowTotalHead-sell">Transaction of Sale</th>
            <th scope="col" colspan="3" class="tableHeadFirst rowTotalHead">All Transactions</th>
          </tr>
          <tr>
            
            <th scope="col" class="tableHeadSecond rowTotalHead-rent">Count</th>
           <th scope="col" class="tableHeadSecond rowTotalHead-rent">Amount</th>
           <th scope="col" class="tableHeadSecond rowTotalHead-rent">Commission</th>
           <th scope="col" class="tableHeadSecond rowTotalHead-sell">Count</th>
           <th scope="col" class="tableHeadSecond rowTotalHead-sell">Amount</th>
           <th scope="col" class="tableHeadSecond rowTotalHead-sell">Commission</th>
           <th scope="col" class="tableHeadSecond rowTotalHead">Count</th>
           <th scope="col" class="tableHeadSecond rowTotalHead">Amount</th>
           <th scope="col" class="tableHeadSecond rowTotalHead">Commission</th>
          
          </tr>
        </thead>
        <tbody>
      <?php } ?>
          <tr>
            <td><?php echo $rowNum ?></td>
            <td><?php echo $row["agent_name"] ?> <br /><span><?php echo $row["SF_agent_ID"] ?></span></td>
            <td class="rowTotal-rent td-price"><?php echo $row["total_rent_count"] ?></td>
            <td class="rowTotal-rent td-price">$<?php echo number_format($row["total_rent_price"]) ?></td>
            <td class="rowTotal-rent td-price">$<?php echo number_format($row["total_rent_comm"]) ?></td>
            <td class="rowTotal-sell td-price"><?php echo $row["total_sell_count"] ?></td>
            <td class="rowTotal-sell td-price">$<?php echo number_format($row["total_sell_price"]) ?></td>
            <td class="rowTotal-sell td-price">$<?php echo number_format($row["total_sell_comm"]) ?></td>
            <td class="rowTotal td-price"><?php echo $row["total_TX_count"] ?></td>
            <td class="rowTotal td-price">$<?php echo number_format($row["total_TX_price"]) ?></td>
            <td class="rowTotal td-price">$<?php echo number_format($row["total_TX_comm"]) ?></td>
          </tr>
      <?php 
        $branch_rent_count += $row["total_rent_count"];
        $branch_rent_total += $row["total_rent_price"];
        $branch_rent_comm += $row["total_rent_comm"];
        $branch_sell_count += $row["total_sell_count"];
        $branch_sell_total += $row["total_sell_price"];
        $branch_sell_comm += $row["total_sell_comm"];
        $branch_total_count += $row["total_TX_count"];
        $branch_total_tx += $row["total_TX_price"];
        $branch_total_comm += $row["total_TX_comm"];
        if ($recordNum == $resultCount) { ?>
      
      <tr class="row-total"> 
        <td class="table-subtotal-title" colspan="2">Branch Total : </td>
        <td class="td-price table-subtotal-rent"><?php echo $branch_rent_count ?></td>
        <td class="td-price table-subtotal-rent">$<?php echo number_format($branch_rent_total) ?></td>
        <td class="td-price table-subtotal-rent">$<?php echo number_format($branch_rent_comm) ?></td>
        <td class="td-price table-subtotal-sell"><?php echo $branch_sell_count ?></td>
        <td class="td-price table-subtotal-sell">$<?php echo number_format($branch_sell_total) ?></td>
        <td class="td-price table-subtotal-sell">$<?php echo number_format($branch_sell_comm) ?></td>
        <td class="td-price table-total"><?php echo $branch_total_count ?></td>
        <td class="td-price table-total">$<?php echo number_format($branch_total_tx) ?></td>
        <td class="td-price table-total">$<?php echo number_format($branch_total_comm) ?></td>
    </tr>

        <?php };
     }?>
        </tbody>
      </table>
      </div>
          <?php
} else {
  echo "<div class='noRecord-box'>Sorry, No records found.</div>";
}
$conn->close();
?>
          </div>
          <!-- Footer -->
<?php include 'footer.php'; ?>
          </body>
        <script>
          $(document.body).on('click', '.viewProperty', function (e) {
            e.preventDefault();
            var cust_id = $(this).data('cust-id');
            var cust_name = $('#' + cust_id).data('cust-name');
            var cust_contact_num = $('#' + cust_id).data('cust-contact-num');
            var district_id = $('#' + cust_id).data('district-id');
            var sub_district_id = $('#' + cust_id).data('sub-district-id');
            var estate_id = $('#' + cust_id).data('estate-id');
            var selling_price = $('#' + cust_id).data('budget-buy');
            var rental_price = $('#' + cust_id).data('budget-rent');
            var propertyType = $(this).data('property-type');
            redirectURL = 'property_list.php?'
                    + 'cust_id=' + cust_id
                    + '&cust_name=' + cust_name
                    + '&cust_contact_num=' + cust_contact_num
                    + '&district_id=' + district_id
                    + '&sub_district_id=' + sub_district_id
                    + '&estate_id=' + estate_id
                    + '&selling_price=' + selling_price
                    + '&rental_price=' + rental_price
                    + '&propertyType=' + propertyType;
            window.location = redirectURL;
          });
        </script>
        </html>
