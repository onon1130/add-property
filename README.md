# README #

## HK Property web-application ##

This is the demonstration web-application of the HK Property system 


### DEMO link ###
https://oscarons.000webhostapp.com/ADD-property/

### Sample Record ###
All the demo record can be found from the database, please import the sql files (mysql/property.sql) and find from database

### Produced for ###

Staffordshire University
BSc (Hons) BIT & IS

Application Design and Development

2017/2018 Course Assessment




### Produced by  ###


Group: G4 

Student ID :  55281668 

Student Name : ON Sin Yu 



### Content source ###

* source code of the web-application
* SQL file of the database (mysql/property.sql)


### Environment Setup ###
Before installation, please install and setup the following with the following configuration

* MySQL 5.6.36 (http://www.mysql.com)
* phpMyAdmin 4.4.15.7 (http://www.phpmyadmin.net)
* Apache/2.4.25 (http://httpd.apache.org/)
* PHP7.0 (http://php.net)

### To install ###

* Download the whole repository
* import the SQL file into phpmyadmin
* start the Apache, PHP and MySQL server
* Run the web-application with a browser
