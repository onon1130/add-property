<?php include 'DB_connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'; ?>
  <body>
    <!-- Navigation -->
    <?php include 'nav.php'; ?>
    <!-- Header - set the background image for the header in the line below -->
    <header class="bg-image-full header-tab">
      <div class="container">
        <div class="header-wrapper">

          <ul class="nav nav-tabs search-tab">
            <li class="nav-item nav-item-wide">
              <a class="nav-link active" data-toggle="tab" data-form-target="property_list.php" href="#searchProperty">Search property <span>from district/estate</span></a>
            </li>
            <li class="nav-item nav-item-wide">
              <a class="nav-link" data-toggle="tab" data-form-target="property_detail.php" href="#searchPropertyID">Search property <span>by property ID</span></a>
            </li>
            <?php if (isset($_SESSION["username"])) { ?>
          <!-- customer search access only for staff -->
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" data-form-target="customer_list.php" href="#searchCustomer">Customer <span>Preference</span></a>
            </li>
            <?php } ?>
          <?php if (isset($_SESSION["username"]) && ( $_SESSION["staffType"] == 'manager' || ($_SESSION["staffType"] == 'admin') )) { ?>
          <!-- branch report access only for branch manager -->
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" data-form-target="branch_report.php" href="#branchReport">Branch <span>Report</span></a>
            </li>
             <?php } ?>
          </ul>
        </div>

      </div>

    </header>
    <form class="search_form"  action="property_list.php" method="POST">
      <div class="tab-content">
        <div class="container">
          <div id="searchProperty" class="tab-pane fade in active show">
           
            <div class="form-group row">
              <div class="col-sm-12 col-md-2 col-form-label">For</div>
              <div class="col-sm-12 col-md-10">
                <row>
                  <div class="form-check ">
                    <label class="form-check-label">
                      <input class="form-check-input" name="propertyType" type="radio" value="rent" checked="">
                      Rental
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" name="propertyType" type="radio" value="sell">
                      Sell
                    </label>
                  </div>
                </row>
              </div>
            </div>
             <div class="form-group row">
              <div class="col-sm-12">
                <i><u>From District</u></i>
              </div>
            </div>
            <div class="form-group row">
              <label for="district" class="col-sm-12 col-md-2 col-form-label">District</label>
              <div class="col-sm-12 col-md-10">
                <row>
                  <div class="col-sm-12 col-md-6 district_selectbox">
                    <select class="form-control" id="district" name="district_id">
                      <option value="">All districts</option>
                      <?php
                      $sql = "SELECT * FROM District";
                      $result = $conn->query($sql);

                      if ($result->num_rows > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {
                          echo "<option value='" . $row["district_ID"] . "'>" . $row["district_name"] . "</option>";
                        }
                      } else {
                        echo "0 results";
                      }
                      
                      ?>
                    </select>
                  </div>

                  <div class="col-sm-12 col-md-6 district_selectbox" id="Subdistrict_container">
                  </div>
                </row>
              </div>
            </div>
            <div class="form-group row">
              <label for="Estate" class="col-sm-12 col-md-2 col-form-label">Select Estate</label>
              <div class="col-sm-12 col-md-10">
                <div id="Estate_container">
                  - All estates -
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12">
                <i><u>Or Input estate name</u></i>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 col-md-2 col-form-label">
                <label for="estate_name">Estate name</labee></div>
              <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" id="estate_name" name="estate_name" placeholder="Input Estate name">
              </div>
            </div>
          </div>
          <div id="searchPropertyID" class="tab-pane fade">
            <div class="form-group row">
             <div class="col-sm-12 col-md-2 col-form-label">
                <label for="property_id">Property ID</label></div>
              <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" id="property_id" name="property_id" placeholder="">
              </div>
            </div>
          </div>
          <?php if (isset($_SESSION["username"])) { ?>
          <!-- customer search access only for staff -->
          <div id="searchCustomer" class="tab-pane fade">

            <div class="form-group row">
              <div class="col-sm-12 col-md-2 col-form-label">
                <label for="cust_name">Customer ID</label></div>
              <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" id="cust_name" name="cust_id" placeholder="">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 col-md-2 col-form-label">
                <label for="cust_name">Customer Name</label></div>
              <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" id="cust_name" name="cust_name" placeholder="">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 col-md-2 col-form-label">
                <label for="cust_contact_num">Contact Number</label></div>
              <div class="col-sm-12 col-md-10">
                <input class="form-control" type="tel" id="cust_contact_num" name="cust_contact_num" placeholder="">
              </div>
            </div>

          </div>
           <?php } ?>
          <?php if (isset($_SESSION["username"]) && ( $_SESSION["staffType"] == 'manager' || ($_SESSION["staffType"] == 'admin') )) { ?>
          <!-- branch report access only for branch manager -->
          <div id="branchReport" class="tab-pane fade">
            <div class="form-group row">
              <label for="branch" class="col-sm-12 col-md-2 col-form-label">Branch</label>
              <div class="col-sm-12 col-md-10">
                <row>
                  <div class="col-sm-12 col-md-6 district_selectbox">
                    <select class="form-control" id="branch" name="branch_id">
                      <option value="">All branches</option>
                      <?php
                      $sql = "SELECT * FROM Branch";
                      $result = $conn->query($sql);

                      if ($result->num_rows > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {
                          echo "<option data-branch-name='" . $row["branch_name"] ."' value='" . $row["branch_ID"] . "'>" . $row["branch_name"] . "</option>";
                        }
                      } else {
                        echo "0 results";
                      }
                      $conn->close();
                      ?>
                    </select>
                    <input id="report_branch_name" type="hidden" name="branch_name" value="">
                  </div>

                </row>
              </div>
            </div>
            <div class="form-group row">
              <label for="reportPeriod" class="col-sm-12 col-md-2 col-form-label">Period</label>
              <div class="col-sm-12 col-md-10">
                <row>
                  <div class="col-sm-12 col-md-6 district_selectbox">
                  <select class="form-control" id="reportPeriod" name="reportPeriod">
                    <option value="">All transactions</option>
                    <option value="currentMonth">This month</option>
                  </select>
                  </div>
              </div>
           </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="button_container">
      <div class="container"><button type="submit" class="btn btn-info btn-lg btn-block">Search</button></div>
    </div>
  </form>
  <!-- Footer -->
  <?php include 'footer.php'; ?>
</body>
<script>

  $(document.body).on('change', '#district', function () {
    var districtID = $(this).val();
    if (districtID) {
      $.ajax({url: "AjaxSelectSubdistrict.php?districtID=" + districtID, cache: false, success: function (result) {
          $('#Subdistrict_container').html(result);
        }});
      $.ajax({url: "AjaxSelectEstate.php?districtID=" + districtID, cache: false, success: function (result) {
          $('#Estate_container').html(result);
        }});
    } else {
      $('#Subdistrict_container').html('');
    }
  });

  $(document.body).on('change', '#subDistrict', function () {
    var subDistrictID = $(this).val();
    if (subDistrictID) {
      $.ajax({url: "AjaxSelectEstate.php?subDistrictID=" + subDistrictID, cache: false, success: function (result) {
          $('#Estate_container').html(result);
        }});
    } else {
      $('#Estate_container').html('- No estate found -');
    }
  });
  $(document.body).on('change', '#branch', function () {
    $('#report_branch_name').val($('#branch option:selected').data('branch-name'));
  });
  $(document.body).on('change', '#estate', function () {
    if ($(this).val()) {
      $('#estate_name').attr('disabled','disabled');
      
    } else {
      $('#estate_name').removeAttr('disabled');
    }
  });
  
  $(document.body).on('click', '.search-tab a', function () {
    var targetURL = $(this).data('form-target');
    console.log(targetURL);
    $('.search_form').attr('action',targetURL);
  });
</script>
</html>


